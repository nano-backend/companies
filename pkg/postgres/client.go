package postgres

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/nano-backend/companies/api"
	"gitlab.com/nano-backend/companies/pkg"
)

type Client struct {
	db *pgxpool.Pool
}

func New(connectionString string) (*Client, error) {
	pool, err := pgxpool.New(context.Background(), connectionString)
	if err != nil {
		return nil, err
	}

	client := &Client{
		db: pool,
	}

	return client, client.Ping()
}

// Postgres error code
const unique_violation = "23505"

func (c *Client) Create(company pkg.Company) (string, error) {
	query := `
	INSERT INTO company (Name, Description, EmployeeCount, Registered, Type)
	VALUES ($1, $2, $3, $4, $5)
	RETURNING ID`
	var id string
	err := c.db.QueryRow(context.Background(), query,
		company.Name, company.Description, company.EmployeeCount, company.Registered, company.Type).
		Scan(&id)
	var pgErr *pgconn.PgError
	if errors.As(err, &pgErr) && pgErr.Code == unique_violation {
		return "", api.ErrNameConflict
	}
	return id, err
}

func (c *Client) Get(companyID string) (pkg.Company, error) {
	query := `
	SELECT ID, Name, Description, EmployeeCount, Registered, Type
	FROM company
	WHERE ID = $1`
	var company pkg.Company
	err := c.db.QueryRow(context.Background(), query, companyID).
		Scan(
			&company.ID,
			&company.Name,
			&company.Description,
			&company.EmployeeCount,
			&company.Registered,
			&company.Type)
	if errors.Is(err, pgx.ErrNoRows) {
		return pkg.Company{}, api.ErrCompanyNotFound
	}
	return company, err
}

func (c *Client) Update(companyID string, company pkg.Company) error {
	query := `
	UPDATE company
	SET 
		Name = $2,
		Description = $3,
		EmployeeCount = $4,
		Registered = $5,
		Type = $6
	WHERE ID = $1`
	tag, err := c.db.Exec(context.Background(), query,
		companyID,
		company.Name,
		company.Description,
		company.EmployeeCount,
		company.Registered,
		company.Type)
	if err != nil {
		return err
	}
	if tag.RowsAffected() != 1 {
		return api.ErrCompanyNotFound
	}
	return nil
}

func (c *Client) Delete(companyID string) error {
	query := "DELETE FROM company WHERE ID = $1"
	tag, err := c.db.Exec(context.Background(), query, companyID)
	if err != nil {
		return err
	}
	if tag.RowsAffected() != 1 {
		return api.ErrCompanyNotFound
	}
	return nil
}

func (c *Client) Ping() error {
	return c.db.Ping(context.Background())
}

func (c *Client) Close() {
	c.db.Close()
}
