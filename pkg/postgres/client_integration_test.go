//go:build integration

package postgres

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/nano-backend/companies/api"
	"gitlab.com/nano-backend/companies/config"
	"gitlab.com/nano-backend/companies/pkg"
)

type Suite struct {
	suite.Suite
	client *Client
}

func TestSuite(t *testing.T) {
	suite.Run(t, new(Suite))
}

func (s *Suite) SetupSuite() {
	config.Initialize()

	var err error
	s.client, err = New(config.PostgresConnectionString)

	s.Suite.Require().NoError(err, "postgres.Client instantiation must not fail")
	s.Suite.Require().NotNil(s.client, "postgres.Client must not be nil")
}

func (s *Suite) TearDownSuite() {
	_, err := s.client.db.Exec(context.Background(), "DELETE FROM company")
	s.Suite.Assert().NoError(err)
	s.client.Close()
}

func (s *Suite) TestCreateCompany() {
	company := pkg.Company{
		Name:          "Create Test",
		Description:   "Create test company",
		EmployeeCount: 90,
		Registered:    true,
		Type:          pkg.NonProfit,
	}
	companyID, err := s.client.Create(company)
	s.Assert().NoError(err)
	s.Assert().NotZero(companyID)

	companyID, err = s.client.Create(company)
	s.Assert().ErrorIs(err, api.ErrNameConflict)
}

func (s *Suite) TestGetCompany() {
	expectedCompany := pkg.Company{
		Name:          "Get Test",
		Description:   "Get test company",
		EmployeeCount: 90,
		Registered:    true,
		Type:          pkg.NonProfit,
	}
	companyID, err := s.client.Create(expectedCompany)
	s.Assert().NoError(err)
	s.Assert().NotZero(expectedCompany)

	expectedCompany.ID = companyID

	company, err := s.client.Get(companyID)
	s.Assert().NoError(err)
	s.Assert().EqualValues(expectedCompany, company)

	// Try to get a company that doesn't exist
	company, err = s.client.Get("04f09e8f-e622-460b-8ff9-517ecf140033")
	s.Assert().ErrorIs(err, api.ErrCompanyNotFound)
}

func (s *Suite) TestUpdateCompany() {
	outdatedCompany := pkg.Company{
		Name:          "Update Test",
		Description:   "Outdated description",
		EmployeeCount: 90,
		Registered:    true,
		Type:          pkg.NonProfit,
	}
	companyID, err := s.client.Create(outdatedCompany)
	s.Assert().NoError(err)

	updatedCompany := pkg.Company{
		ID:            companyID,
		Name:          "Update Test",
		Description:   "New description",
		EmployeeCount: 900,
		Registered:    true,
		Type:          pkg.NonProfit,
	}

	err = s.client.Update(companyID, updatedCompany)
	s.Assert().NoError(err)

	expectedCompany, err := s.client.Get(companyID)
	s.Assert().NoError(err)
	s.Assert().EqualValues(expectedCompany, updatedCompany)

	// Try to update a company that doesn't exist
	err = s.client.Update("04f09e8f-e622-460b-8ff9-517ecf140033", updatedCompany)
	s.Assert().ErrorIs(err, api.ErrCompanyNotFound)
}

func (s *Suite) TestDeleteCompany() {
	deletableCompany := pkg.Company{
		Name:          "Delete Test",
		Description:   "Description",
		EmployeeCount: 90,
		Registered:    true,
		Type:          pkg.NonProfit,
	}
	companyID, err := s.client.Create(deletableCompany)
	s.Assert().NoError(err)

	err = s.client.Delete(companyID)
	s.Assert().NoError(err)

	// Try to delete a company that doesn't exist
	err = s.client.Delete(companyID)
	s.Assert().ErrorIs(err, api.ErrCompanyNotFound)
}
