package pkg

type Company struct {
	ID            string
	Name          string
	Description   string
	EmployeeCount int
	Registered    bool
	Type          CompanyType
}

type CompanyType string

const (
	Corporation        CompanyType = "Corporation"
	NonProfit          CompanyType = "NonProfit"
	Cooperative        CompanyType = "Cooperative"
	SoleProprietorship CompanyType = "Sole Proprietorship"
)

type CompanyDataSource interface {
	Create(company Company) (string, error)
	Get(companyID string) (Company, error)
	Update(companyID string, company Company) error
	Delete(companyID string) error
	Ping() error
}

type DataSource interface {
	Ping() error
}

type MockCompanyDataSource struct {
	Mock_Create func(company Company) (string, error)
	Mock_Get    func(companyID string) (Company, error)
	Mock_Update func(companyID string, company Company) error
	Mock_Delete func(companyID string) error
	Mock_Ping   func() error
}

func (m *MockCompanyDataSource) Create(company Company) (string, error) {
	return m.Mock_Create(company)
}

func (m *MockCompanyDataSource) Get(companyID string) (Company, error) {
	return m.Mock_Get(companyID)
}

func (m *MockCompanyDataSource) Update(companyID string, company Company) error {
	return m.Mock_Update(companyID, company)
}

func (m *MockCompanyDataSource) Delete(companyID string) error {
	return m.Mock_Delete(companyID)
}

func (m *MockCompanyDataSource) Ping() error {
	return m.Mock_Ping()
}
