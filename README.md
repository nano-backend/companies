# Golang Exercise

Companies microservice

## Environment variables

| Variable             | Description                                      | Default Value                                                  |
|:---------------------|:-------------------------------------------------|:---------------------------------------------------------------|
| LOG_LEVEL            | Levels: debug, info                              | info                                                           |
| LISTEN_ADDRESS       | REST Service handler address                     | 0.0.0.0:80                                                     |
| POSTGRES_CONN_STRING | PostgreSQL connection string                     | postgres://postgres:postgres@postgres/postgres?sslmode=disable |
| JWT_SECRET           | Symmetric key used to sign and verify JWT tokens |                                                                |
| KAFKA_SERVICE        | Kafka broker address, comma separated            | kafka:9092                                                     |

If environment variables are not set, the application will attempt to load them from the file `.env` present in its current working directory.

If no file is found, the application will use the default values.

There is no default value for `JWT_SECRET`, must be manually set.

If `LOG_LEVEL` set to `debug`, the application will generate and print a JWT token that can be used to test the endpoints.

## Endpoints

| Action         | HTTP Method | Endpoint          | Request Body | Response                             |
|:---------------|:------------|:------------------|:-------------|:-------------------------------------|
| Create Company | `POST`      | `/company`        | Company JSON | `Location: /company/{uuid}` (header) |
| Get Company    | `GET`       | `/company/{uuid}` |              | Company JSON                         |
| Update Company | `PUT`       | `/company/{uuid}` | Company JSON |                                      |
| Delete Company | `DELETE`    | `/company/{uuid}` |              |                                      |

Company JSON example:

```json
{
  "ID": "04f09e8f-e622-460b-8ff9-517ecf140033",
  "Name": "New",
  "Description": "New company",
  "EmployeeCount": 100,
  "Registered": true,
  "Type": "Sole Proprietorship"
}
```

Kafka events are produced for `POST`, `PUT` and `DELETE` requests. The event value is the request's URL path.

---

### Local Development and Tests

Assuming a production scenario where the repository is configured with a CI/CD pipeline that runs lints, builds the docker image and runs the integration tests.

Below is described the setup of the local development enviroment.

---

Create a file named `.env` in the root of the project to override the default values:

```ini
LOG_LEVEL=debug
LISTEN_ADDRESS=localhost:8080
POSTGRES_CONN_STRING=postgres://postgres:postgres@localhost/postgres?sslmode=disable
JWT_SECRET=secret
KAFKA_SERVICE=localhost:9094
```

The default values are intended to minimize comfiguration setup in production.

Prepare the testing environment:

```shell
docker compose up
```

This will start up a PostgreSQL database and a Kafka broker ready to receive requests.

Now you can run:

```shell
go test -tags integration ./...
```

To build a docker image with the compiled application run:

```shell
docker build -t company .
```

## Post deadline refactoring

Check out the [refactored](https://gitlab.com/nano-backend/companies/-/tree/refactored) branch for a revised codebase with readability improvements and tests with better code coverage.
