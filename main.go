package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/nano-backend/companies/api/rest"
	"gitlab.com/nano-backend/companies/config"
	"gitlab.com/nano-backend/companies/pkg/postgres"
)

func main() {
	config.Initialize()

	dataSource, err := postgres.New(config.PostgresConnectionString)
	if err != nil {
		log.Fatal(err)
	}
	defer dataSource.Close()

	rest.StartService(config.ListenAddress, config.JWTSecret, config.KafkaBrokers, dataSource)
	defer rest.Stop()

	if config.LogLevel == "debug" {
		printDebugJWTToken()
	}

	waitForExitSignal()
}

func waitForExitSignal() {
	waitChan := make(chan os.Signal, 1)
	signal.Notify(waitChan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-waitChan
}

func printDebugJWTToken() {
	tokenAuth := jwtauth.New("HS256", []byte(config.JWTSecret), nil)
	_, tokenString, err := tokenAuth.Encode(map[string]interface{}{"user_id": 1})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("DEBUG: Use this JWT token: \n%s\n", tokenString)
}
