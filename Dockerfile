FROM golang:1.21 as builder

RUN curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.54.0

WORKDIR /usr/src/app

COPY . .

RUN go mod download && go mod verify

RUN set -e; \
	go vet ./...; \
	golangci-lint run

RUN CGO_ENABLED=0 go build -o ./bin/companies -a -ldflags '-extldflags "-static"' .

FROM scratch

COPY --from=builder /usr/src/app/bin/companies .

USER 1000

EXPOSE 80

CMD [ "/companies" ]
