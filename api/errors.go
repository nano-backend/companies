package api

import "errors"

var ErrInvalidCompanyFormat = errors.New("invalid company format")
var ErrNameConflict = errors.New("company with the same name already registered")
var ErrCompanyNotFound = errors.New("company not found")
