//go:build integration

package rest

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/jwtauth/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/nano-backend/companies/config"
	"gitlab.com/nano-backend/companies/pkg/postgres"
)

func TestService(t *testing.T) {
	config.Initialize()

	tokenAuth := jwtauth.New("HS256", []byte(config.JWTSecret), nil)
	_, tokenString, err := tokenAuth.Encode(map[string]interface{}{"user_id": 1})
	require.NoError(t, err)

	dataSource, err := postgres.New(config.PostgresConnectionString)
	require.NoError(t, err)
	defer dataSource.Close()

	router := buildRouter(tokenAuth, config.KafkaBrokers, dataSource)

	t.Run("Fail Authentication", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodDelete, "/company/04f09e8f-e622-460b-8ff9-517ecf140033", nil)
		w := httptest.NewRecorder()
		router.ServeHTTP(w, r)
		result := w.Result()

		assert.Equal(t, http.StatusUnauthorized, result.StatusCode)

		responseBody, _ := io.ReadAll(result.Body)
		println(string(responseBody))
		println(result.Status)
	})

	t.Run("Authenticated but not found", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodDelete, "/company/04f09e8f-e622-460b-8ff9-517ecf140033", nil)
		r.Header.Set("Authorization", fmt.Sprintf("Bearer %v", tokenString))

		w := httptest.NewRecorder()
		router.ServeHTTP(w, r)
		result := w.Result()

		assert.Equal(t, http.StatusNotFound, result.StatusCode)
	})
}
