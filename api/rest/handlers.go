package rest

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/nano-backend/companies/api"
	"gitlab.com/nano-backend/companies/pkg"
)

type companyHandler struct {
	dataSource pkg.CompanyDataSource
}

func (h *companyHandler) getCompanyHandler(w http.ResponseWriter, r *http.Request) {
	companyID := chi.URLParam(r, "uuid")

	company, err := h.dataSource.Get(companyID)
	if err != nil {
		log.Println(err)
		if errors.Is(err, api.ErrCompanyNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintf(w, "error: %s", err)
		return
	}

	body, err := json.MarshalIndent(company, "", "\t")
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(body)
}

func (h *companyHandler) createCompanyHandler(w http.ResponseWriter, r *http.Request) {
	var company pkg.Company
	if err := json.NewDecoder(r.Body).Decode(&company); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "error: %s", api.ErrInvalidCompanyFormat)
		return
	}

	id, err := h.dataSource.Create(company)
	if err != nil {
		log.Println(err)
		if errors.Is(err, api.ErrNameConflict) {
			w.WriteHeader(http.StatusConflict)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintf(w, "error: %s", err)
		return
	}

	w.Header().Add("Location", fmt.Sprintf("/company/%s", id))
	w.WriteHeader(http.StatusCreated)
}

func (h *companyHandler) updateCompanyHandler(w http.ResponseWriter, r *http.Request) {
	companyID := chi.URLParam(r, "uuid")

	var company pkg.Company
	if err := json.NewDecoder(r.Body).Decode(&company); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "error: %s", api.ErrInvalidCompanyFormat)
		return
	}

	err := h.dataSource.Update(companyID, company)
	if err != nil {
		log.Println(err)
		if errors.Is(err, api.ErrCompanyNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintf(w, "error: %s", err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}

func (h *companyHandler) deleteCompanyHandler(w http.ResponseWriter, r *http.Request) {
	companyID := chi.URLParam(r, "uuid")

	err := h.dataSource.Delete(companyID)
	if err != nil {
		log.Println(err)
		if errors.Is(err, api.ErrCompanyNotFound) {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintf(w, "error: %s", err)
		return
	}

	w.WriteHeader(http.StatusAccepted)
}
