package rest

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/IBM/sarama"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"gitlab.com/nano-backend/companies/pkg"
)

var server *http.Server
var producer sarama.SyncProducer

func StartService(listenAddress string, jwtKey string, kafkaBrokers []string, dataSource pkg.CompanyDataSource) {
	tokenAuth := jwtauth.New("HS256", []byte(jwtKey), nil)

	router := buildRouter(tokenAuth, kafkaBrokers, dataSource)
	server = &http.Server{Addr: listenAddress, Handler: router}
	spawnHttpListener()
}

func buildRouter(tokenAuth *jwtauth.JWTAuth, kafkaBrokers []string, dataSource pkg.CompanyDataSource) http.Handler {
	handler := companyHandler{dataSource: dataSource}

	router := chi.NewRouter()
	router.Get("/company/{uuid}", handler.getCompanyHandler)

	router.Group(func(router chi.Router) {
		router.Use(jwtauth.Verifier(tokenAuth))
		router.Use(jwtauth.Authenticator)

		router.Use(kafkaNotifier(kafkaBrokers))

		router.Post("/company", handler.createCompanyHandler)
		router.Put("/company/{uuid}", handler.updateCompanyHandler)
		router.Delete("/company/{uuid}", handler.deleteCompanyHandler)
	})

	router.Get("/{health_check}", healthProbeHandler(dataSource))
	return router
}

func spawnHttpListener() {
	log.Printf("listening for connections on: %s", server.Addr)
	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatal(err)
		}
		log.Println(http.ErrServerClosed)
	}()
}

func Stop() {
	if err := server.Shutdown(context.Background()); err != nil {
		log.Println(err)
	}
	if err := producer.Close(); err != nil {
		log.Println(err)
	}
}

func healthProbeHandler(dataSource pkg.DataSource) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		switch healthCheck := chi.URLParam(r, "health_check"); healthCheck {
		case "live", "livez":
			w.WriteHeader(http.StatusOK)
		case "ready", "readyz":
			if dataSource != nil || dataSource.Ping() != nil {
				w.WriteHeader(http.StatusServiceUnavailable)
			} else {
				w.WriteHeader(http.StatusOK)
			}
		default:
			w.WriteHeader(http.StatusNotFound)
		}
	}
}

func kafkaNotifier(brokers []string) func(next http.Handler) http.Handler {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 10
	config.Producer.Return.Successes = true

	var err error
	producer, err = sarama.NewSyncProducer(brokers, config)
	if err != nil {
		log.Fatalln("Failed to start Sarama producer:", err)
	}

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			message := sarama.ProducerMessage{
				Topic: "company",
				Value: sarama.StringEncoder(r.URL.Path),
			}
			if _, _, err := producer.SendMessage(&message); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "Failed to store your data: %s", err)
			}
			next.ServeHTTP(w, r)
		})
	}
}
