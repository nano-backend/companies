package rest

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nano-backend/companies/api"
	"gitlab.com/nano-backend/companies/pkg"
)

func TestPostHandlers(t *testing.T) {
	type test struct {
		company                pkg.Company
		expectedStatusCode     int
		expectedLocationHeader string
		expectedResponseBody   string
	}

	tests := map[string]test{
		"Unique name conflict": {
			company: pkg.Company{
				Name:          "Repeated",
				Description:   "Repeated company",
				EmployeeCount: 100,
				Registered:    true,
				Type:          pkg.SoleProprietorship,
			},
			expectedStatusCode:   http.StatusConflict,
			expectedResponseBody: "error: company with the same name already registered",
		},
		"Success": {
			company: pkg.Company{
				Name:          "New",
				Description:   "New company",
				EmployeeCount: 100,
				Registered:    true,
				Type:          pkg.SoleProprietorship,
			},
			expectedStatusCode:     http.StatusCreated,
			expectedLocationHeader: "/company/00000000-0000-0000-0000-000000000001",
			expectedResponseBody:   "",
		},
	}

	dataSource := &pkg.MockCompanyDataSource{
		Mock_Create: func(company pkg.Company) (string, error) {
			if company.Name == "Repeated" {
				return "", api.ErrNameConflict
			}
			return "00000000-0000-0000-0000-000000000001", nil
		},
	}

	restHandler := companyHandler{dataSource: dataSource}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			body, _ := json.Marshal(test.company)
			r := httptest.NewRequest(http.MethodPost, "/company", bytes.NewReader(body))

			w := httptest.NewRecorder()
			restHandler.createCompanyHandler(w, r)

			responseBody, _ := io.ReadAll(w.Result().Body)
			assert.Equal(t, test.expectedStatusCode, w.Result().StatusCode)
			assert.Equal(t, test.expectedLocationHeader, w.Result().Header.Get("Location"))
			assert.Equal(t, test.expectedResponseBody, string(responseBody))
		})
	}
}

func TestGetHandlers(t *testing.T) {
	type test struct {
		companyID            string
		expectedStatusCode   int
		expectedResponseBody string
	}

	tests := map[string]test{
		"Unknown Company": {
			companyID:            "00000000-0000-0000-0000-000000000002",
			expectedStatusCode:   http.StatusNotFound,
			expectedResponseBody: "error: company not found",
		},
		"Success": {
			companyID:          "00000000-0000-0000-0000-000000000001",
			expectedStatusCode: http.StatusOK,
			expectedResponseBody: `{
	"ID": "00000000-0000-0000-0000-000000000001",
	"Name": "New",
	"Description": "New company",
	"EmployeeCount": 100,
	"Registered": true,
	"Type": "Sole Proprietorship"
}`,
		},
	}

	dataSource := &pkg.MockCompanyDataSource{
		Mock_Get: func(companyID string) (pkg.Company, error) {
			if companyID != "00000000-0000-0000-0000-000000000001" {
				return pkg.Company{}, api.ErrCompanyNotFound
			}
			return pkg.Company{
				ID:            "00000000-0000-0000-0000-000000000001",
				Name:          "New",
				Description:   "New company",
				EmployeeCount: 100,
				Registered:    true,
				Type:          pkg.SoleProprietorship,
			}, nil
		},
	}

	restHandler := companyHandler{dataSource: dataSource}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {

			r := httptest.NewRequest(http.MethodGet, "/company/{uuid}", nil)
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("uuid", test.companyID)
			r = r.WithContext(context.WithValue(r.Context(), chi.RouteCtxKey, rctx))

			w := httptest.NewRecorder()
			restHandler.getCompanyHandler(w, r)

			responseBody, _ := io.ReadAll(w.Body)
			assert.Equal(t, test.expectedStatusCode, w.Result().StatusCode)
			assert.Equal(t, test.expectedResponseBody, string(responseBody))
		})
	}
}

func TestUpdateHandler(t *testing.T) {
	type test struct {
		company              pkg.Company
		expectedStatusCode   int
		expectedResponseBody string
	}

	tests := map[string]test{
		"Unknown Company": {
			company: pkg.Company{
				ID:            "00000000-0000-0000-0000-000000000002",
				Name:          "Unknown",
				Description:   "Unknown Company",
				EmployeeCount: 0,
				Registered:    false,
				Type:          pkg.Corporation,
			},
			expectedStatusCode:   http.StatusNotFound,
			expectedResponseBody: "error: company not found",
		},
		"Success": {
			company: pkg.Company{
				ID:            "00000000-0000-0000-0000-000000000001",
				Name:          "Company",
				Description:   "Success Company",
				EmployeeCount: 999,
				Registered:    false,
				Type:          pkg.SoleProprietorship,
			},
			expectedStatusCode:   http.StatusAccepted,
			expectedResponseBody: "",
		},
	}

	dataSource := &pkg.MockCompanyDataSource{
		Mock_Update: func(companyID string, company pkg.Company) error {
			if companyID != "00000000-0000-0000-0000-000000000001" {
				return api.ErrCompanyNotFound
			}
			return nil
		},
	}

	restHandler := companyHandler{dataSource: dataSource}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {

			body, _ := json.Marshal(test.company)
			r := httptest.NewRequest(http.MethodPut, "/company/{uuid}", bytes.NewReader(body))
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("uuid", test.company.ID)
			r = r.WithContext(context.WithValue(r.Context(), chi.RouteCtxKey, rctx))

			w := httptest.NewRecorder()
			restHandler.updateCompanyHandler(w, r)

			responseBody, _ := io.ReadAll(w.Body)
			assert.Equal(t, test.expectedStatusCode, w.Result().StatusCode)
			assert.Equal(t, test.expectedResponseBody, string(responseBody))
		})
	}
}

func TestDeleteHandler(t *testing.T) {
	type test struct {
		companyID            string
		expectedStatusCode   int
		expectedResponseBody string
	}

	tests := map[string]test{
		"Unknown Company": {
			companyID:            "00000000-0000-0000-0000-000000000002",
			expectedStatusCode:   http.StatusNotFound,
			expectedResponseBody: "error: company not found",
		},
		"Success": {
			companyID:            "00000000-0000-0000-0000-000000000001",
			expectedStatusCode:   http.StatusAccepted,
			expectedResponseBody: "",
		},
	}

	dataSource := &pkg.MockCompanyDataSource{
		Mock_Delete: func(companyID string) error {
			if companyID != "00000000-0000-0000-0000-000000000001" {
				return api.ErrCompanyNotFound
			}
			return nil
		},
	}

	restHandler := companyHandler{dataSource: dataSource}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {

			r := httptest.NewRequest(http.MethodPut, "/company/{uuid}", nil)
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("uuid", test.companyID)
			r = r.WithContext(context.WithValue(r.Context(), chi.RouteCtxKey, rctx))

			w := httptest.NewRecorder()
			restHandler.deleteCompanyHandler(w, r)

			responseBody, _ := io.ReadAll(w.Body)
			assert.Equal(t, test.expectedStatusCode, w.Result().StatusCode)
			assert.Equal(t, test.expectedResponseBody, string(responseBody))
		})
	}
}
