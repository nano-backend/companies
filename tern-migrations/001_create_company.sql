CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TYPE company_type AS ENUM ('Corporation', 'NonProfit', 'Cooperative', 'Sole Proprietorship');

CREATE TABLE company (
  ID UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4 (),
  Name VARCHAR (15) NOT NULL UNIQUE,
  Description VARCHAR (3000),
  EmployeeCount NUMERIC NOT NULL,
  Registered BOOLEAN NOT NULL,
  Type company_type NOT NULL
);

---- create above / drop below ----

DROP TABLE company;

DROP TYPE company_type;
