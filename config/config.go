package config

import (
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

var (
	// LOG_LEVEL
	LogLevel = "info"

	// LISTEN_ADDRESS
	ListenAddress = "0.0.0.0:80"

	// POSTGRES_CONN_STRING
	PostgresConnectionString = "postgres://postgres:postgres@postgres/postgres?sslmode=disable"

	// JWT_SECRET
	JWTSecret = ""

	// KAFKA_SERVICE
	KafkaBrokers = []string{"kafka:9092"}
)

// Initialize attempts to override the default config values by the ones listed in the .env file
func Initialize() {
	_ = godotenv.Load(".env")

	// This enables integration tests call config.Initialize() and read the .env in the root of the project
	_ = godotenv.Load("../../.env")
	_ = godotenv.Load("../.env")

	// Make sure so secrets are left in the enviroment variables
	defer os.Clearenv()

	if value, exists := os.LookupEnv("LOG_LEVEL"); exists {
		LogLevel = value
	}

	if value, exists := os.LookupEnv("LISTEN_ADDRESS"); exists {
		ListenAddress = value
	}

	if value, exists := os.LookupEnv("POSTGRES_CONN_STRING"); exists {
		PostgresConnectionString = value
	}

	if value, exists := os.LookupEnv("JWT_SECRET"); exists {
		JWTSecret = value
	} else {
		log.Fatalln("Value for configuration variable JWT_SECRET must be provided")
	}

	if value, exists := os.LookupEnv("KAFKA_SERVICE"); exists {
		KafkaBrokers = strings.Split(value, ",")
	}
}
